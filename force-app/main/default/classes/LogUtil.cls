/**
@author:            Stephen Laylo
@company:           J4RVIS
@description:       Utility to log errors and exceptions to a custom object across the application
@param:            logLevel         - Debug, Error, Info, Warning
@param:            sourceClass      - Originating apex class
@param:            sourceFunction   - Method in class above that initiated the log
@param:            ex               - The standard exception object for errors
@param:            message          - The user friendly message associated with the log record
@param:            referenceId      - The salesforce record or job ID related to the log
@param:            referenceInfo    - Additional context about the reference ID (e.g. Apex Batch, Web Callout, Contact etc,,,)
@param:            payLoad          - Optional payload. E.g. could be used to log the Request and Response of a web callout
@param:            logCode          - Optional logCode. Used for reporting purposes
*/
public without sharing class LogUtil {
    
    //store an instance of the class (singleton)
    private static LogUtil instance = null;
    
    //store the configured logging states
    private Log_Setting__mdt  logLevels;
    private Boolean errorLoggingIsEnabled;
    private Boolean debugLoggingIsEnabled;
    private Boolean infoLoggingIsEnabled;
    private Boolean warnLoggingIsEnabled;
    private static List<String> fieldNames = new List<String> {
        'Log_Level__c',
        'Source__c',
        'Source_Function__c',
        'Message__c',
        'Reference_ID__c',
        'Reference_Info__c',
        'Payload__c',
        'Log_Code__c',
        'Stack_Trace__c',
        'Exception_Type__c'
    };
                
    //store the logs that will be created
    private List<Log__c> logs = new List<Log__c>();
    
    /**
    @author:          Stephen Laylo
    @company:         J4RVIS
    @description:     Constructor to create an instance of the logging utility.
                      This will be invoked by calling the LogUtil.getInstance() method using the singleton pattern
    */
    private LogUtil() {
        //gets the debugging config stored in custom metadata
        logLevels = [
            SELECT Enable_Debug_Logging__c,
            Enable_Error_Logging__c,
            Enable_Warn_Logging__c,
            Enable_Info_Logging__c
            FROM Log_Setting__mdt 
            WHERE DeveloperName = 'Log_Level'
            LIMIT 1
        ];
        
        this.errorLoggingIsEnabled = logLevels != null ? logLevels.Enable_Error_Logging__c : false;
        this.debugLoggingIsEnabled = logLevels != null ? logLevels.Enable_Debug_Logging__c : false;
        this.infoLoggingIsEnabled = logLevels != null ? logLevels.Enable_Info_Logging__c : false;
        this.warnLoggingIsEnabled = logLevels != null ? logLevels.Enable_Warn_Logging__c : false;
    }
    
    /**
    @author:          Stephen Laylo
    @company:         J4RVIS
    @description:     Singleton pattern to lazy load logLevels
    */
    public static LogUtil getInstance() {
        if (instance == null) {
            instance = new LogUtil();
        }

        return instance;
    }
    
    /**
    @author:           Stephen Laylo
    @company:           J4RVIS
    @description:       Inner class that utilises builder pattern to create a log record
    */
    private without sharing class LogBuilder {
        
        private String logLevel;
        private String sourceClass;
        private String sourceFunction;
        private String message;
        private String referenceId;
        private String referenceInfo;
        private String payLoad;
        private String logCode;
        private String stackTrace;
        private Exception ex;
        
        /**
        @author:          Stephen Laylo
        @company:         J4RVIS
        @description:     Adds core values to log builder instance variables
        */
        private LogBuilder addLog(
            String sourceClass,
            String sourceFunction,
            String message,
            String referenceId,
            String referenceInfo,
            String payLoad,
            String logCode) {
                this.sourceClass = sourceClass;
                this.sourceFunction = sourceFunction;
                this.message = message;
                this.referenceId = referenceId;
                this.referenceInfo = referenceInfo;
                this.payLoad = payLoad;
                this.logCode = logCode;
                
                return this;
            }
        
        /**
        @author:          Stephen Laylo
        @company:         J4RVIS
        @description:     Adds log level to log builder instance variable
        */
        private LogBuilder addLogLevel(String logLevel) {
            this.logLevel = logLevel;

            return this;
        }
        
        /**
        @author:          Stephen Laylo
        @company:         J4RVIS
        @description:     Adds exception to log builder instance variables
        */
        private LogBuilder addException(Exception ex) {
            this.ex = ex;

            return this;
        }
        
        /**
        @author:          Stephen Laylo
        @company:         J4RVIS
        @description:     insatiates an Log__c objects
        */
        private Log__c generate() {
            Log__c logEntry  = new Log__c();
            
            if (this.logLevel != null) {
                logEntry.Log_Level__c = this.logLevel;
            }

            if (this.sourceClass != null) {
                logEntry.Source__c = this.sourceClass;
            }

            if (this.sourceFunction != null) {
                logEntry.Source_Function__c = this.sourceFunction;
            }

            if (this.message != null) {
                logEntry.Message__c = this.message;
            }

            if (this.referenceId != null) {
                logEntry.Reference_ID__c = this.referenceId;
            }

            if (this.referenceInfo != null) {
                logEntry.Reference_Info__c = this.referenceInfo;  
            }

            if (this.payLoad != null) {
                logEntry.Payload__c = this.payLoad;
            }

            if (this.logCode != null) {
                logEntry.Log_Code__c = this.logCode;
            }

            if (this.ex != null) {
                logEntry.Stack_Trace__c = this.ex.getStackTraceString();
                logEntry.Exception_Type__c = this.ex.getTypeName();
            }

            return logEntry;
        }
    }
    
    /**
    @author:          Stephen Laylo
    @company:         J4RVIS
    @description:     Method to add a log record.
    */
    public LogUtil addLog(
            String sourceClass,
            String sourceFunction,
            Exception ex,
            String message,
            String referenceID,
            String referenceInfo,
            String payLoad,
            String logCode,
            String logLevel) {
        
        Log__c logEntry;
        
        LogBuilder log = new LogBuilder()
            .addLog(sourceClass,
                    sourceFunction,
                    message,
                    referenceId,
                    referenceInfo,
                    payLoad,
                    logCode);
        
        // Used to log exceptions that need support team visibility
        // (e.g. a controller class failed to update a record)
        if (logLevel == Constants.LOG_LEVEL_ERROR && errorLoggingIsEnabled) {
            log.addLogLevel(Constants.LOG_LEVEL_ERROR)
                .addException(ex);
            logEntry = log.generate();
        }
        
        // Used to log a statement that will assist with issue resolution
        // (e.g. log the request and response from a web service callout)
        if (logLevel == Constants.LOG_LEVEL_DEBUG && debugLoggingIsEnabled) {
            log.addLogLevel(Constants.LOG_LEVEL_DEBUG);
            logEntry = log.generate();
        }
        
        // Used to log information about a process
        // (e.g. when a batch job finished, how many records were processed,
        // how many batches were used, what time did the job start and finish)
        if (logLevel == Constants.LOG_LEVEL_INFO && infoLoggingIsEnabled) {
            log.addLogLevel(Constants.LOG_LEVEL_INFO);
            logEntry = log.generate();
        }
        
        // Used to notify the support team that one or more application limits are in danger of being reached
        // (e.g. an Account trigger results in 80 SOQL queries)
        if (logLevel == Constants.LOG_LEVEL_WARN && warnLoggingIsEnabled) {
            log.addLogLevel(Constants.LOG_LEVEL_WARN);
            logEntry = log.generate();
        }
        
        //if the logEntry isn't null add it to the list of logs
        if (logEntry != null) {
            logs.add(logEntry);
        }

        return this;
    }
    
    /**
    @author:          Stephen Laylo
    @company:         J4RVIS
    @description:     Method to create/insert all log records.
	*/
    public List<Log__c> createLogs(Boolean persist) {
        if (logs.size() > 0) {
            if (persist) {
                upsert logs;
            }
        }

        return logs;
    }
}