public class Constants {

    //Account
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_ORGANISATION = 'Organisation';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_NATIONAL_COMMITTEE = 'National_Committee';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_INTERNATIONAL_COMMITTEE = 'International_Committee';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_INTERNATIONAL_Body = 'International_Body';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_Household = 'Household';

    public static final String ACCOUNT_SA_STATUS_ACTIVE = 'Active';
    public static final String ACCOUNT_COMMITTEE_STATUS_ACTIVE = 'Active';
    public static final String ACCOUNT_COMMITTEE_TYPE_SC = 'SC';
    public static final String ACCOUNT_COMMITTEE_TYPE_WG = 'WG';
    public static final String ACCOUNT_COMMITTEE_TYPE_TC = 'TC';

    //Generate XML
    public static final String GENERATEXML_RECORDTYPE_FIELD_ORGANISATION = 'Organisiation';
    public static final String GENERATEXML_RECORDTYPE_FIELD_NATIONAL_COMMITTEE = 'National Committee';

    //Committee Role
    public static final String COMMITTEEROLE_ROLE_STATUS_CURRENT = 'Current';
    public static final String COMMITTEEROLE_ROLE_STATUS_FORMER = 'Former';
    public static final String COMMITTEEROLE_ROLE_STATUS_AWAITING_ASSIGNMENT = 'Awaiting Assignment';
    

    //Contact
    public static final String CONTACT_SA_STATUS_ACTIVE = 'Active';

    //User
    public static final String GROUP_DEVELOPERNAME_SYSTEM_USER_QUEUE = 'System_User_Queue';
    public static final String GROUP_TYPE_QUEUE = 'Queue';
    public static final String USER_LICENSE_SF = 'Salesforce';
    public static final String USER_LICENSE_SF_PLATFORM = 'Salesforce Platform';

    //Logs
    public static final String LOG_LEVEL_ERROR = 'Error';
    public static final String LOG_LEVEL_DEBUG = 'Debug';
    public static final String LOG_LEVEL_INFO = 'Info';
    public static final String LOG_LEVEL_WARN = 'Warning';
    public static final String LOG_UTIL_RESPONSE_SUCCESS = 'Success';
    public static final String LOG_UTIL_RESPONSE_INVALID_REQUEST = 'Invalid Request';
    public static final String LOG_UTIL_RESPONSE_UNAUTHORIZED = 'Unauthorized';
    public static final String LOG_UTIL_RESPONSE_INTERNAL_SERVER_ERROR = 'Internal Server Error';
    public static final String LOG_UTIL_RESPONSE_UNEXPECTED_SYSTEM_ERROR = 'Unexpected system error or system timeout';
    public static final String LOG_UTIL_RESPONSE_UNKNOWN_ERROR = 'Unknown Error';
    public static final String LOG_UTIL_RESPONSE_EXCEPTION = 'Exception';

}