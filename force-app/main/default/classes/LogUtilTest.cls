/**
    @author:            Jannis Bott
    @company:           J4RVIS
    @description:       Test class for the error logging utility class
*/
@isTest(seeAllData=false)
private class LogUtilTest {

    //test that error logs can be created
    private static testMethod void testErrorLogCreate() {

        List<Log__c> logs = LogUtil.getInstance()
                .addLog('sourceClass',
                        'sourceFunction',
                        new CustomException('Error Exception'),
                        'message',
                        'referenceID',
                        'referenceInfo',
                        'payLoad',
                        'logCode',
                        Constants.LOG_LEVEL_ERROR)
                .createLogs(false);

        System.assertEquals(true, logs.size() == 1, 'The number of logs created should be 1');
        System.assertEquals('sourceClass', logs[0].Source__c, 'The source should contain \'sourceClass\'');
    }

    //test that debug logs can be created
    private static testMethod void testDebugLogCreate() {

        List<Log__c> logs = LogUtil.getInstance()
                .addLog('debugSourceClass',
                        'sourceFunction',
                        null,
                        'message',
                        'referenceID',
                        'referenceInfo',
                        'payLoad',
                        'logCode',
                        Constants.LOG_LEVEL_DEBUG)
                .createLogs(false);

        System.assertEquals(true, logs.size() == 1, 'The number of logs created should be 1');
        System.assertEquals('debugSourceClass', logs[0].Source__c, 'The source should contain \'debugSourceClass\'');
    }

    //test that warn logs can be created
    private static testMethod void testWarnLogCreate() {

        List<Log__c> logs = LogUtil.getInstance()
                .addLog('warnSourceClass',
                        'sourceFunction',
                        null,
                        'message',
                        'referenceID',
                        'referenceInfo',
                        'payLoad',
                        'logCode',
                        Constants.LOG_LEVEL_WARN)
                .createLogs(false);

        System.assertEquals(true, logs.size() == 1, 'The number of logs created should be 1');
        System.assertEquals('warnSourceClass', logs[0].Source__c, 'The source should contain \'warnSourceClass\'');
    }

    //test that info logs can be created
    private static testMethod void testInfoLogCreate() {

        List<Log__c> logs = LogUtil.getInstance()
                .addLog('infoSourceClass',
                        'sourceFunction',
                        null,
                        'message',
                        'referenceID',
                        'referenceInfo',
                        'payLoad',
                        'logCode',
                        Constants.LOG_LEVEL_INFO)
                .createLogs(false);

        System.assertEquals(true, logs.size() == 1, 'The number of logs created should be 1');
        System.assertEquals('infoSourceClass', logs[0].Source__c, 'The source should contain \'infoSourceClass\'');
    }

    //test that logs cannot be created and that errors are handled gracefully
    private static testMethod void testInfoLogInsertException() {

        List<Log__c> logs = LogUtil.getInstance()
                .addLog('infoSourceClass',
                        'sourceFunction',
                        null,
                        'message',
                        'referenceID',
                        'referenceInfo',
                        'payLoad',
                        'logCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCodelogCode',
                        'Non Existing Log Level')
                .createLogs(true);
 
        System.debug(logs);
        System.assertEquals(true, logs.isEmpty(), 'No logs should have been inserted and the Id should be null');
    }

    //test that error logs can be inserted
    private static testMethod void testErrorLogInsert() {

        List<Log__c> logs = LogUtil.getInstance()
                .addLog('sourceClass',
                        'sourceFunction',
                        new CustomException('Error Exception'),
                        'message',
                        'referenceID',
                        'referenceInfo',
                        'payLoad',
                        'logCode',
                        Constants.LOG_LEVEL_ERROR)
                .createLogs(true);

        List<Log__c> queriedLogs = [SELECT Source__c FROM Log__c];
        System.assertEquals(true, queriedLogs.size() == 1, 'The number of logs inserted should be 1');
        System.assertEquals('sourceClass', queriedLogs[0].Source__c, 'The source should contain \'sourceClass\'');
    }

    //test that multiple logs can be created and inserted
    private static testMethod void testMultipleErrorLogInsert() {

        LogUtil.getInstance()
                .addLog('sourceClass',
                        'sourceFunction',
                        new CustomException('Error Exception'),
                        'message',
                        'referenceID',
                        'referenceInfo',
                        'payLoad',
                        'logCode',
                        Constants.LOG_LEVEL_ERROR)
                .addLog('sourceClass2',
                        'sourceFunction2',
                        new CustomException('Error Exception2'),
                        'message2',
                        'referenceID2',
                        'referenceInfo2',
                        'payLoad2',
                        'logCode2',
                        Constants.LOG_LEVEL_ERROR);

        List<Log__c> logs = LogUtil.getInstance()
                .addLog('sourceClass3',
                        'sourceFunction3',
                        new CustomException('Error Exception3'),
                        'message3',
                        'referenceID3',
                        'referenceInfo3',
                        'payLoad3',
                        'logCode3',
                        Constants.LOG_LEVEL_ERROR)
                .createLogs(true);

        List<Log__c> queriedLogs = [SELECT Source__c FROM Log__c];
        System.assertEquals(true, queriedLogs.size() == 3, 'The number of logs inserted should be 3');
    }

    private class CustomException extends Exception {}
}