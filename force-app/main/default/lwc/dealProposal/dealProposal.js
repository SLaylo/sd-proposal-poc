import { LightningElement, track, api } from 'lwc';
import { reduceErrors } from 'c/ldsUtils';

export default class DealProposal extends LightningElement {
    
    @track isRendering = false;
    @track error;

    /* VARIABLES FOR CONFIGURATIONS FOR LABELS */
    @api proposalQuestion; // = 'What are you wanting to propose?';
    @api publicationTypeQuestion; // = 'What type of publication are you proposing?';
    @api relatedToAnyExistingPublicationsQuestion; // = 'Does your proposal relate to any existing publications?';
    @api multipleRelatedPublicationsSearchQuestion; //= 'Search for multiple related publications';
    @api proposedPublicationTitleQuestion; // = 'What will the title of your proposed publication be?';
    @api problemDesignedToSolveQuestion; // = 'What problem is this designed to solve?';
    @api proposedPublicationScopeQuestion; // = 'What is the scope of the new publication? (be as detailed & comprehensive as possible)';
    @api revisePublicationsSearchQuestion; // = 'Search for publications you want to revise/amend';
    @api revisePublicationDesignationTitleQuestion; // = 'Enter Designation & Title of publication you want to revise/amend';
    @api currentEditionIssuesQuestion; // = 'What are the issues with the current edition?';
    @api evidenceExistsProblemQuestion; // = 'What evidence exists to demonstrate there is a problem?';
    @api proposedSolutionQuestion; // = 'What is your proposed solution (be as detailed & comprehensive as possible)?';
    @api multipleInternationalPublicationsSearchQuestion; // = 'Search for multiple international publications';
    @api adoptInternationalStandardDesignationTitleQuestion; // = 'Enter Designation & Title of International Standard you want to adopt';
    @api adoptionIdenticalOrModifyContentQuestion; // = 'Is this adoption identical, or do you need to modify the content?';
    @api whyPublicationBeAdoptedInAustraliaQuestion; // = 'Why should this publication be adopted in Australia?';
    @api whyCannotAdoptedIdenticallyCurrentInternationalStandardQuestion; // = 'Why can’t the current international standard be adopted identically (refer to TBT agreement information)';
    @api proposedVariationsQuestion; // = 'What are your proposed variations (be as detailed as possible)';
    @api addMoreProjectsQuestion; // = 'Do you want to add more projects to your proposal?';
    @api benefitsToAustralianCommunityPublicHealthAndSafetyQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Public and health safety?';
    @api benefitsToAustralianCommunitySocialAndCommunityImpactQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Social and community impact?';
    @api benefitsToAustralianCommunityEnvironmentalImpactQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Environmental impact?';
    @api benefitsToAustralianCommunityCompetitionQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Competition?';
    @api benefitsToAustralianCommunityEconomicImpactQuestion; // = 'How will this work bring Net Benefit to the Australian Community - Economic impact?';
    @api consultedPeopleQuestion; // = 'Who has been consulted and what are their views?';
    @api declarationText; // = 'Review and agree to Declaration (including Privacy Notice)';
    @api doNotAgreeText; // = 'You are required to agree with the declaration terms before you can submit this proposal. If you do not agree to the terms please contact a SEM to discuss: www.standards.org.au/engagementevents/ sectors';
    @api thankYouText; // = 'Thank you for submitting your proposal';

    /* VARIABLES FOR FIELDS */
    @track proposal;
    @track publicationType;
    @track isRelatedToAnyExistingPublications;
    @track selectedRelatedPublications;
    @track proposedPublicationTitle;
    @track problemDesignedToSolve;
    @track proposedPublicationScope;
    @track selectedPublicationsToBeRevised;
    @track revisedPublicationDesignation;
    @track revisedPublicationTitle;
    @track currentEditionIssues;
    @track evidenceExistsProblem;
    @track proposedSolution;
    @track selectedInternationalPublications;
    @track adoptedPublicationDesignation;
    @track adoptedPublicationTitle;
    @track adoptionIdenticalOrModifyContent;
    @track whyPublicationBeAdoptedInAustraliaReason;
    @track whyCannotAdoptedIdenticallyCurrentInternationalStandardReason;
    @track proposedVariations;
    @track willAddMoreProjects;
    @track benefitsToAustralianCommunityPublicHealthAndSafety;
    @track benefitsToAustralianCommunitySocialAndCommunityImpact;
    @track benefitsToAustralianCommunityEnvironmentalImpact;
    @track benefitsToAustralianCommunityCompetition;
    @track benefitsToAustralianCommunityEconomicImpact;
    @track consultedPeople;
    @track disclaimerCheck = false;

    /* SCREENS */
    @track proposalScreen = true;
    @track createNewPublicationScreen1 = false;
    @track createNewPublicationScreen2 = false;
    @track amendExistingPublicationsScreen1 = false;
    @track amendExistingPublicationsScreen2 = false;
    @track adoptInternationalPublicationsScreen1 = false;
    @track adoptInternationalPublicationsScreen2 = false;
    @track addMoreProposalScreen = false;
    @track netBenefitScreen1 = false;
    @track netBenefitScreen2 = false;
    @track reviewAllProposedProjectsScreen = false;
    @track declarationAndPrivacyNoticeScreen = false;
    @track thankYouScreen = false;

    proposalChoices = {
        data: {
            values: [
                {
                    label: 'Create a New Publication',
                    value: 'Create a New Publication'
                },
                {
                    label: 'Revise/amend existing publications',
                    value: 'Revise/amend existing publications'
                },
                {
                    label: 'Adopt international publications',
                    value: 'Adopt international publications'
                }
            ]
        }
    };

    publicationTypeChoices = {
        data: {
            values: [
                {
                    label: 'Handbook',
                    value: 'Handbook'
                },
                {
                    label: 'Interim Standard',
                    value: 'Interim Standard'
                },
                {
                    label: 'Miscellaneous Publication Standard',
                    value: 'Miscellaneous Publication Standard'
                },
                {
                    label: 'Standard',
                    value: 'Standard'
                },
                {
                    label: 'Supplement',
                    value: 'Supplement'
                },
                {
                    label: 'Technical Report',
                    value: 'Technical Report'
                },
                {
                    label: 'Technical Specification',
                    value: 'Technical Specification'
                },
                {
                    label: 'Don’t know',
                    value: 'Don’t know'
                }
            ]
        }
    };

    relatedToAnyExistingPublicationsChoices = {
        data: {
            values: [
                {
                    label: 'Yes',
                    value: 'Yes'
                },
                {
                    label: 'No',
                    value: 'No'
                }
            ]
        }
    };

    addMoreProjectsChoices = {
        data: {
            values: [
                {
                    label: 'Yes',
                    value: 'Yes'
                },
                {
                    label: 'No',
                    value: 'No'
                }
            ]
        }
    };

    adoptionIdenticalOrModifyContentChoices = {
        data: {
            values: [
                {
                    label: 'Identical',
                    value: 'Identical'
                },
                {
                    label: 'Modified',
                    value: 'Modified'
                }
            ]
        }
    };

    disclaimerCheckChoices = {
        data: {
            values: [
                {
                    label: 'Agree',
                    value: 'Agree'
                },
                {
                    label: 'Disagree',
                    value: 'Disagree'
                }
            ]
        }
    };

    relatedPublicationsChoices = {
        data: {
            values: [
                {
                    label: 'My Publication 1',
                    value: 'My Publication 1'
                },
                {
                    label: 'My Publication 2',
                    value: 'My Publication 2'
                },
                {
                    label: 'My Publication 3',
                    value: 'My Publication 3'
                }
            ]
        }
    };

    publicationsToBeRevisedChoices = {
        data: {
            values: [
                {
                    label: 'My Publication 1',
                    value: 'My Publication 1'
                },
                {
                    label: 'My Publication 2',
                    value: 'My Publication 2'
                }
            ]
        }
    };

    internationalPublicationsChoices = {
        data: {
            values: [
                {
                    label: 'My International Publication 1',
                    value: 'My International Publication 1'
                },
                {
                    label: 'My International Publication 2',
                    value: 'My International Publication 2'
                },
                {
                    label: 'My International Publication 3',
                    value: 'My International Publication 3'
                },
                {
                    label: 'My International Publication 4',
                    value: 'My International Publication 4'
                },
                {
                    label: 'My International Publication 5',
                    value: 'My International Publication 5'
                }
            ]
        }
    };

    connectedCallback() {
        this.initializeValues();
    }

    initializeValues() {
        this.proposal = null;
        this.publicationType = null;
        this.isRelatedToAnyExistingPublications = null;
        this.selectedRelatedPublications = null;
        this.proposedPublicationTitle = null;
        this.problemDesignedToSolve = null;
        this.proposedPublicationScope = null;
        this.selectedPublicationsToBeRevised = null;
        this.revisedPublicationDesignation = null;
        this.revisedPublicationTitle = null;
        this.currentEditionIssues = null;
        this.evidenceExistsProblem = null;
        this.proposedSolution = null;
        this.selectedInternationalPublications = null;
        this.adoptedPublicationDesignation = null;
        this.adoptedPublicationTitle = null;
        this.adoptionIdenticalOrModifyContent = null;
        this.whyPublicationBeAdoptedInAustraliaReason = null;
        this.whyCannotAdoptedIdenticallyCurrentInternationalStandardReason = null;
        this.proposedVariations = null;
        this.willAddMoreProjects = null;
        this.benefitsToAustralianCommunityPublicHealthAndSafety = null;
        this.benefitsToAustralianCommunitySocialAndCommunityImpact = null;
        this.benefitsToAustralianCommunityEnvironmentalImpact = null;
        this.benefitsToAustralianCommunityCompetition = null;
        this.benefitsToAustralianCommunityEconomicImpact = null;
        this.consultedPeople = null;
        this.disclaimerCheck = false;
    }

    handleProposalChange(event) {
        this.proposal = event.detail.value;
    }

    handlePublicationTypeChange(event) {
        this.publicationType = event.detail.value;
    }

    handleRelatedToAnyExistingPublicationsChange(event) {
        this.isRelatedToAnyExistingPublications = event.detail.value;
    }

    handleRelatedPublicationsSelect(event) {
        this.selectedRelatedPublications = event.detail.value;
    }

    handlePublicationsToBeRevisedSelect(event) {
        this.selectedPublicationsToBeRevised = event.detail.value;
    }

    handleInternationalPublicationsSelect(event) {
        this.selectedInternationalPublications = event.detail.value;
    }

    handleAdoptionIdenticalOrModifyContentOnChange(event) {
        this.adoptionIdenticalOrModifyContent = event.detail.value;
    }

    handleAddMoreProjectsChange(event) {
        this.willAddMoreProjects = event.detail.value;
    }

    proposedPublicationTitleOnBlur(event) {
        this.proposedPublicationTitle = event.target.value;
    }

    problemDesignedToSolveOnBlur(event) {
        this.problemDesignedToSolve = event.target.value;
    }

    proposedPublicationScopeOnBlur(event) {
        this.proposedPublicationScope = event.target.value;
    }

    currentEditionIssuesOnBlur(event) {
        this.currentEditionIssues = event.target.value;
    }

    evidenceExistsProblemOnBlur(event) {
        this.evidenceExistsProblem = event.target.value;
    }

    proposedSolutionOnBlur(event) {
        this.proposedSolution = event.target.value;
    }

    whyPublicationBeAdoptedInAustraliaReasonOnBlur(event) {
        this.whyPublicationBeAdoptedInAustraliaReason = event.target.value;
    }

    whyCannotAdoptedIdenticallyCurrentInternationalStandardReasonOnBlur(event) {
        this.whyCannotAdoptedIdenticallyCurrentInternationalStandardReason = event.target.value;
    }

    proposedVariationsOnBlur(event) {
        this.proposedVariations = event.target.value;
    }

    benefitsToAustralianCommunityPublicHealthAndSafetyOnBlur(event) {
        this.benefitsToAustralianCommunityPublicHealthAndSafety = event.target.value;
    }

    benefitsToAustralianCommunitySocialAndCommunityImpactOnBlur(event) {
        this.benefitsToAustralianCommunitySocialAndCommunityImpact = event.target.value;
    }

    benefitsToAustralianCommunityEnvironmentalImpactOnBlur(event) {
        this.benefitsToAustralianCommunityEnvironmentalImpact = event.target.value;
    }

    benefitsToAustralianCommunityCompetitionOnBlur(event) {
        this.benefitsToAustralianCommunityCompetition = event.target.value;
    }

    benefitsToAustralianCommunityEconomicImpactOnBlur(event) {
        this.benefitsToAustralianCommunityEconomicImpact = event.target.value;
    }

    consultedPeopleOnBlur(event) {
        this.consultedPeople = event.target.value;
    }

    proposedPublicationTitleOnChange(event) {
        this.proposedPublicationTitle = event.target.value;
    }

    problemDesignedToSolveOnChange(event) {
        this.problemDesignedToSolve = event.target.value;
    }

    proposedPublicationScopeOnChange(event) {
        this.proposedPublicationScope = event.target.value;
    }

    currentEditionIssuesOnChange(event) {
        this.currentEditionIssues = event.target.value;
    }

    evidenceExistsProblemOnChange(event) {
        this.evidenceExistsProblem = event.target.value;
    }

    proposedSolutionOnChange(event) {
        this.proposedSolution = event.target.value;
    }

    whyPublicationBeAdoptedInAustraliaReasonOnChange(event) {
        this.whyPublicationBeAdoptedInAustraliaReason = event.target.value;
    }

    whyCannotAdoptedIdenticallyCurrentInternationalStandardReasonOnChange(event) {
        this.whyCannotAdoptedIdenticallyCurrentInternationalStandardReason = event.target.value;
    }

    proposedVariationsOnChange(event) {
        this.proposedVariations = event.target.value;
    }

    benefitsToAustralianCommunityPublicHealthAndSafetyOnChange(event) {
        this.benefitsToAustralianCommunityPublicHealthAndSafety = event.target.value;
    }

    benefitsToAustralianCommunitySocialAndCommunityImpactOnChange(event) {
        this.benefitsToAustralianCommunitySocialAndCommunityImpact = event.target.value;
    }

    benefitsToAustralianCommunityEnvironmentalImpactOnChange(event) {
        this.benefitsToAustralianCommunityEnvironmentalImpact = event.target.value;
    }

    benefitsToAustralianCommunityCompetitionOnChange(event) {
        this.benefitsToAustralianCommunityCompetition = event.target.value;
    }

    benefitsToAustralianCommunityEconomicImpactOnChange(event) {
        this.benefitsToAustralianCommunityEconomicImpact = event.target.value;
    }

    consultedPeopleOnChange(event) {
        this.consultedPeople = event.target.value;
    }

    handleDisclaimerCheck(event) {
        this.disclaimerCheck = event.target.value;
    }

    handleBackButton(event) {
        if (this.createNewPublicationScreen1 === true) {
            this.proposalScreen = true;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.createNewPublicationScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = true;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.amendExistingPublicationsScreen1 === true) {
            this.proposalScreen = true;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.amendExistingPublicationsScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = true;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.adoptInternationalPublicationsScreen1 === true) {
            this.proposalScreen = true;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.adoptInternationalPublicationsScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = true;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.addMoreProposalScreen === true) {
            if (this.proposal == 'Create a New Publication') {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = true;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen3 = false;
                this.adoptInternationalPublicationsScreen4 = false;
                this.adoptInternationalPublicationsScreen5 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;

            } else if (this.proposal == 'Revise/amend existing publications') {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = true;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen3 = false;
                this.adoptInternationalPublicationsScreen4 = false;
                this.adoptInternationalPublicationsScreen5 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;

            } else if (this.proposal == 'Adopt international publications') {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = true;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;
            }

        } else if (this.netBenefitScreen1 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = true;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.netBenefitScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = true;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.reviewAllProposedProjectsScreen === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = true;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.declarationAndPrivacyNoticeScreen === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = true;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.thankYouScreen === true) {
            this.proposalScreen = true;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
        }
    }

    handleCancelProposalButton(event) {
        
    }

    handleSaveDraftProposalButton(event) {
        
    }

    handleNextButton(event) {
        if (this.proposalScreen === true) {
            if (this.proposal == 'Create a New Publication') {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = true;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;

            } else if (this.proposal == 'Revise/amend existing publications') {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = true;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;

            } else if (this.proposal == 'Adopt international publications') {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = true;
                this.adoptInternationalPublicationsScreen2 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;
            }

        } else if (this.createNewPublicationScreen1 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = true;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.createNewPublicationScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = true;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.amendExistingPublicationsScreen1 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = true;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.amendExistingPublicationsScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = true;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.adoptInternationalPublicationsScreen1 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = true;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.adoptInternationalPublicationsScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = true;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;
            
        } else if (this.addMoreProposalScreen === true) {
            if (this.willAddMoreProjects == 'Yes') {
                this.proposalScreen = true;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = false;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;
            
                this.initializeValues();

            } else if (this.willAddMoreProjects == 'No') {
                this.proposalScreen = false;
                this.createNewPublicationScreen1 = false;
                this.createNewPublicationScreen2 = false;
                this.amendExistingPublicationsScreen1 = false;
                this.amendExistingPublicationsScreen2 = false;
                this.adoptInternationalPublicationsScreen1 = false;
                this.adoptInternationalPublicationsScreen2 = false;
                this.addMoreProposalScreen = false;
                this.netBenefitScreen1 = true;
                this.netBenefitScreen2 = false;
                this.reviewAllProposedProjectsScreen = false;
                this.declarationAndPrivacyNoticeScreen = false;
                this.thankYouScreen = false;
            }

        } else if (this.netBenefitScreen1 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = true;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.netBenefitScreen2 === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = true;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = false;

        } else if (this.reviewAllProposedProjectsScreen === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = true;
            this.thankYouScreen = false;

        }
    }

    handleSubmitProposalButton(event) {
        if (this.declarationAndPrivacyNoticeScreen === true) {
            this.proposalScreen = false;
            this.createNewPublicationScreen1 = false;
            this.createNewPublicationScreen2 = false;
            this.amendExistingPublicationsScreen1 = false;
            this.amendExistingPublicationsScreen2 = false;
            this.adoptInternationalPublicationsScreen1 = false;
            this.adoptInternationalPublicationsScreen2 = false;
            this.addMoreProposalScreen = false;
            this.netBenefitScreen1 = false;
            this.netBenefitScreen2 = false;
            this.reviewAllProposedProjectsScreen = false;
            this.declarationAndPrivacyNoticeScreen = false;
            this.thankYouScreen = true;
        }
            
        this.initializeValues();
    }

    get hasRelatedAnyExistingPublications() {
        return (this.isRelatedToAnyExistingPublications == 'Yes' ? true : false);
    }

    get isAdoptionModifiedContent() {
        return (this.adoptionIdenticalOrModifyContent == 'Modified' ? true : false);
    }

    get isDisclaimerChecked() {
        return (this.disclaimerCheck == 'Agree' ? true : false);
    }

    get isDisclaimerDisagree() {
        return (this.disclaimerCheck == 'Disagree' ? true : false);
    }

    get isProposalCreateNewPublication() {
        return (this.proposal == 'Create a New Publication');
    }

    get isProposalAmendingExistingPublications() {
        return (this.proposal == 'Revise/amend existing publications');
    }

    get isProposalAdoptInternationalPublications() {
        return (this.proposal == 'Adopt international publications');
    }

    errorCallback(error, stack) {
        this.error = reduceErrors(error);

        // console.log('errorCallback -> reduceErrors(error) -> ', reduceErrors(error));
        // console.log('errorCallback -> error -> ', error);
        // console.log('errorCallback -> stack -> ', stack);
    }

}